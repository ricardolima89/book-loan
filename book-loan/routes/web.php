<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Books Controller Routing
|--------------------------------------------------------------------------
|
| Routing to Books application
|
*/
$app->get('/', function () use ($app) {
    return view('index');
});

$app->get('/books', 'BooksController@index');

$app->get('/books/{id:[\d]+}', [
    'as' => 'books.show',
    'uses' => 'BooksController@show'
]);

$app->post('/books', 'BooksController@store');

$app->put('/books/{id:[\d]+}', 'BooksController@update');

$app->delete('/books/{id:[\d]+}', 'BooksController@destroy');

/*
|--------------------------------------------------------------------------
| Friend Controller Route
|--------------------------------------------------------------------------
|
| Routing to Friends application
|
*/
$app->group([
    'prefix' => '/friends'
], function () use ($app) {

    $app->get('/', 'FriendsController@index');

    $app->post('/', 'FriendsController@store');

    $app->get('/{id:[\d]+}', [
        'as' => 'friends.show',
        'uses' => 'FriendsController@show'
    ]);

    $app->put('/{id:[\d]+}', 'FriendsController@update');

    $app->delete('/{id:[\d]+}', 'FriendsController@destroy');
});