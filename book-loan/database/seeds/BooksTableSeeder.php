<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Friend::class, 10)->create()->each(function($friend){
            $friend->book()->save(factory(App\Book::class)->make());
        });
    }
}
