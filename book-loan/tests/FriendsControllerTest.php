<?php

namespace Tests\App\Http\Controllers;

use TestCase;
use Carbon\Carbon;
use Laravel\Lumen\Testing\DatabaseMigrations;

class FriendsControllerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test **/
    public function index_responds_with_200_status_code()
    {
        $response = $this->call('GET', '/friends');

        $this->assertEquals(200, $response->status());
    }

    /** @test **/
    public function index_should_return_a_collection_of_records()
    {
        $friends = factory('App\Friend', 2)->create();

        $this->get('/friends', ['Accept' => 'application/json']);

        $body = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('data', $body);
        $this->assertCount(2, $body['data']);

        foreach ($friends as $friend) {
            $this->seeJson([
                'id' => $friend->id,
                'name' => $friend->name,
                'created' => $friend->created_at->toIso8601String(),
                'updated' => $friend->updated_at->toIso8601String(),
            ]);
        }
    }

    /** @test **/
    public function show_should_return_a_valid_friend()
    {
        $book = $this->bookFactory();
        $friend = $book->friend;

        $this->get("/friends/{$friend->id}", ['Accept' => 'application/json']);
        $body = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('data', $body);

        $this->seeJson([
            'id' => $friend->id,
            'name' => $friend->name,
            'created' => $friend->created_at->toIso8601String(),
            'updated' => $friend->updated_at->toIso8601String(),
        ]);
    }

    /** @test **/
    public function show_should_fail_on_an_invalid_friend()
    {
        $this
            ->get('/friends/999999')
            ->seeStatusCode(404)
            ->seeJson([
                'error' => [
                    'message' => 'Friend not found'
                ]
            ]);
    }

    /** @test **/
    public function show_optionally_includes_books()
    {
        $book = $this->bookFactory();
        $friend = $book->friend;

        $this->get(
            "/friends/{$friend->id}?include=books",
            ['Accept' => 'application/json']
        );

        $body = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('data', $body);
        $data = $body['data'];
        $this->assertArrayHasKey('books', $data);
        $this->assertArrayHasKey('data', $data['books']);
        $this->assertCount(1, $data['books']['data']);

        // See Friend Data
        $this->seeJson([
            'id' => $friend->id,
            'name' => $friend->name,
        ]);

        // Test included book Data (the first record)
        $actual = $data['books']['data'][0];
        $this->assertEquals($book->id, $actual['id']);
        $this->assertEquals($book->title, $actual['title']);
        $this->assertEquals(
            $book->created_at->toIso8601String(),
            $actual['created']
        );
        $this->assertEquals(
            $book->updated_at->toIso8601String(),
            $actual['updated']
        );
    }

    /** @test **/
    public function store_can_create_a_new_friend()
    {
        $postData = ['name' => 'H. G. Wells'];

        $this->post('/friends', $postData, ['Accept' => 'application/json']);

        $this->assertEquals(201, $this->response->status());

        $data = $this->response->getData(true);

        $this->assertArrayHasKey('data', $data);
        $this->seeJson($postData);
        $this->seeInDatabase('friends', $postData);
    }

    /** @test **/
    public function store_returns_a_valid_location_header()
    {
        $postData = ['name' => 'H. G. Wells'];

        $this->post('/friends', $postData, ['Accept' => 'application/json']);

        $this->assertEquals(201, $this->response->status());

        $data = $this->response->getData(true);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('id', $data['data']);

        // Check the Location header
        $id = $data['data']['id'];
        $this->seeHeaderWithRegExp('Location', "#/friends/{$id}$#");
    }

    /** @test **/
    public function update_can_update_an_existing_friend()
    {
        $friend = factory('App\Friend')->create([
            'name' => 'Old Friend Name'
        ]);

        $requestData = ['name' => 'New Friend Name'];

        $this
            ->put(
                "/friends/{$friend->id}",
                $requestData,
                ['Accept' => 'application/json']
            )
            ->seeStatusCode(200)
            ->seeJson($requestData)
            ->seeInDatabase('friends', [
                'name' => 'New Friend Name'
            ])
            ->notSeeInDatabase('friends', [
                'name' => $friend->name
            ]);

        $this->assertArrayHasKey('data', $this->response->getData(true));
    }

    /** @test **/
    public function delete_can_remove_a_friend()
    {
        $friend = factory(\App\Friend::class)->create();
        $this
            ->delete("/friends/{$friend->id}")
            ->seeStatusCode(204)
            ->notSeeInDatabase('friends', ['id' => $friend->id]);
    }

    /** @test **/
    public function deleting_an_invalid_friend_should_return_a_404()
    {
        $this
            ->delete('/friends/99999', [],
                ['Accept' => 'application/json'])
            ->seeStatusCode(404);
    }
}