<?php

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    /**
     * See if the response has a header.
     *
     * @param $header
     * @return $this
     */
    public function seeHasHeader($header)
    {
        $this->assertTrue(
            $this->response->headers->has($header),
            "Response should have the header '{$header}' but does not."
        );

        return $this;
    }

   /**
     * Asserts that the response header matches a given regular expression
     *
     * @param $header
     * @param $regexp
     * @return $this
     */
    public function seeHeaderWithRegexp($header, $regexp)
    {
        $this
            ->seeHasHeader($header)
            ->assertRegexp(
                $regexp,
                $this->response->headers->get($header)
            );
        return $this;
    }

    protected function bookFactory($count = 1)
    {
        if ($count === 1) {
            $friend = factory('App\Friend')->create();
            $books  = factory('App\Book')->make();
            $books->friend()->associate($friend);
            $books->save();
        } else {
            $books  = factory('App\Book',$count)->make();
            foreach ($books as $book) {
                $friend = factory('App\Friend')->create();
                $book->friend()->associate($friend);
                $book->save();
            }
        }

        return $books;
    }
}
