<?php

namespace Tests\App\Http\Controllers;

use TestCase;
use Carbon\Carbon;
use Laravel\Lumen\Testing\DatabaseMigrations;

class BooksControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        Carbon::setTestNow(Carbon::now('UTC'));
    }

    public function tearDown()
    {
        parent::tearDown();
        Carbon::setTestNow();
    }

    /** @test **/
    public function index_status_code_should_be_200()
    {
        $response = $this->call('GET', '/books');

        $this->assertEquals(200, $response->status());
    }

    /** @test **/
    public function index_should_return_book_collection()
    {
        $books = factory('App\Book', 2)->create();

        $this->get('/books');

        $content = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        foreach ($books as $book) {
            $this->seeJson(['title' => $book->title]);
        }
    }

    /** @test **/
    public function index_should_return_book_collection_with_friends()
    {
        $books = $this->bookFactory(2);
        $this->get('/books');

        $content = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        foreach ($books as $book) {
            $this->seeJson([
                'title' => $book->title,
                'friend' => $book->friend->name
            ]);
        }
    }

    /** @test **/
    public function show_should_return_a_valid_book()
    {
        $book = factory('App\Book')->create();
        $this
            ->get("/books/{$book->id}")
            ->seeStatusCode(200)
            ->seeJson([
                'id' => $book->id,
                'title' => $book->title
            ]);

        $content = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertEquals($book->id, $data['id']);
        $this->assertEquals($book->title, $data['title']);
        $this->assertEquals($book->friend, $data['friend']);
        $this->assertEquals($book->created_at->toIso8601String(), $data['created']);
        $this->assertEquals($book->updated_at->toIso8601String(), $data['created']);
    }

    /** @test **/
    public function show_should_return_a_valid_book_with_friend()
    {
        $book = $this->bookFactory(1);

        $this
            ->get("/books/{$book->id}")
            ->seeStatusCode(200)
            ->seeJson([
                'id' => $book->id,
                'title' => $book->title
            ]);

        $content = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        $data = $content['data'];
        $this->assertEquals($book->id, $data['id']);
        $this->assertEquals($book->title, $data['title']);
        $this->assertEquals($book->friend->name, $data['friend']);
        $this->assertEquals($book->created_at->toIso8601String(), $data['created']);
        $this->assertEquals($book->updated_at->toIso8601String(), $data['created']);
    }

    /** @test **/
    public function show_should_fail_when_book_id_doesnt_exist()
    {
        $this
            ->get('/books/999999')
            ->seeStatusCode(404)
            ->seeJson([
                'error' => [
                    'message' => 'Book not found'
                ]
            ]);
    }

    /** @test **/
    public function show_route_should_not_match_a_valid_route()
    {
        $this->get('/books/isso-eh-invalido');

        $this->assertNotRegexp(
            '/Book not found/',
            $this->response->getContent(),
            'BooksController@show route matching when it should not.'
        );
    }

    /** @test **/
    public function store_should_save_new_book_in_the_database()
    {
        $this->post('/books', [
            'title' => 'Star Wars: Rogue One'
        ]);

        $body = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('data', $body);

        $data = $body['data'];
        $this->assertEquals('Star Wars: Rogue One', $data['title']);

        $this->assertTrue($data['id'] > 0, 'Expected a positive integer, but did not see one.');
        $this->assertArrayHasKey('created', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['created']);
        $this->assertArrayHasKey('updated', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['updated']);
        $this->seeInDatabase('books', ['title' => 'Star Wars: Rogue One']);
    }

    /** @test **/
    public function store_should_save_new_book_with_friend_in_database()
    {
        $friend = factory('App\Friend')->create([
            'name' => 'Arnold Schwarzenegger'
        ]);

        $this->post('/books', [
            'title' => 'Terminator',
            'friend_id' => $friend->id
        ], ['Accept' => 'application/json']);

        $body = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('data', $body);
        $data = $body['data'];
        $this->assertEquals('Terminator', $data['title']);
        $this->assertEquals('Arnold Schwarzenegger', $data['friend']);
        $this->assertArrayHasKey('created', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['created']);
        $this->assertArrayHasKey('updated', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['updated']);
        $this->seeInDatabase('books', ['title' => 'Terminator']);
    }

    /** @test **/
    public function store_should_respond_with_201_and_location_header_when_success()
    {
        $this->post('/books', [
            'title' => 'Foundation'
        ]);

        $this
            ->seeStatusCode(201)
            ->seeHeaderWithRegexp('Location', '#/books/[\d]+$#');
    }

    /** @test **/
    public function update_should_only_change_fillable_fields()
    {
        $book = factory('App\Book')->create([
            'title' => 'Foundation'
        ]);

        $this->put("/books/{$book->id}", [
            'id' => 5,
            'title' => 'Foundation 2'
        ]);

        $this
            ->seeStatusCode(200)
            ->seeJson([
                'id' => 1,
                'title' => 'Foundation 2'
            ])
            ->seeInDatabase('books', [
                'title' => 'Foundation 2'
            ]);

        $body = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('data', $body);

        $data = $body['data'];
        $this->assertArrayHasKey('created', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['created']);
        $this->assertArrayHasKey('updated', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['updated']);
    }

    /** @test **/
    public function update_should_fail_with_invalid_id()
    {
        $this
            ->put('/books/9999999999')
            ->seeStatusCode(404)
            ->seeJsonEquals([
                'error' => [
                    'message' => 'Book not found'
                ]
            ]);
    }

    /** @test **/
    public function update_should_not_match_invalid_route()
    {
        $this->put('/books/eh-invalido')
            ->seeStatusCode(404);
    }

    /** @test **/
    public function destroy_should_remove_valid_book()
    {
        $book = factory('App\Book')->create();
        $this
            ->delete("/books/{$book->id}")
            ->seeStatusCode(204)
            ->isEmpty();

        $this->notSeeInDatabase('books', ['id' => $book->id]);
    }
}
