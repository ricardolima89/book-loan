<?php

namespace Tests\App\Transformer;

use TestCase;
use Mockery as m;
use App\Transformer\FriendTransformer;
use Laravel\Lumen\Testing\DatabaseMigrations;

class FriendTransformerTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->subject = new FriendTransformer();
    }

    /** @test **/
    public function it_can_be_initialized()
    {
        $this->assertInstanceOf(FriendTransformer::class, $this->subject);
    }

    /** @test **/
    public function it_can_transform_a_friend()
    {
        $friend = factory(\App\Friend::class)->create();

        $actual = $this->subject->transform($friend);

        $this->assertEquals($friend->id, $actual['id']);
        $this->assertEquals($friend->name, $actual['name']);
        $this->assertEquals(
            $friend->created_at->toIso8601String(),
            $actual['created']
        );
        
        $this->assertEquals(
            $friend->updated_at->toIso8601String(),
            $actual['created']
        );
    }

    /** @test **/
    public function it_can_transform_related_book()
    {
        $book = $this->bookFactory();
        $friend = $book->friend;
        $data = $this->subject->includeBooks($friend);
        $this->assertInstanceOf(\League\Fractal\Resource\Collection::class, $data);
    }
}