<!DOCTYPE html>
<html lang="en-US" ng-app="bookLoan">
<head>
    <title>Book lending manager</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- Load Angular JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <!-- Load Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Book APP</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li id="books-menu-item" class="active" data-target="#booksDiv"><a href="#">Books <span class="sr-only">(current)</span></a></li>
                    <li id="friends-menu-item" data-target="#friendsDiv"><a href="#">Friends</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div  id="booksDiv" class="section" ng-controller="booksCtrl">

        <!-- Data table -->
        <table class="table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add',0)">Add New Book</button></th>
                </tr>
              </thead>
              <tbody>
                 <tr ng-repeat="book in books">
                    <td>{{ $index + 1 }}</td>
                    <td>{{ book.title }} <span class="badge" ng-if="book.friend !== null"> {{book.friend}} has it!</span></td>
                    <td>
                       <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit',book.id)">Edit</button>
                       <button class="btn btn-danger btn-xs btn-delete" ng-click="confirmDelete(book.id)">Delete</button>
                    </td>
                </tr>
            </tbody>
        </table>

        <!-- Modal for book creating and editing -->
        <div class="modal fade" id="bookModal" tabindex="-1" role="dialog" aria-labelledby="bookModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="bookModalLabel">{{state}}</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="inputTitle" class="col-sm-3 control-label">Title</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="inputTitle" required placeholder="Book title" value="{{title}}" ng-model="formData.title">
                                </div>
                                <label ng-if="!hasFriend && state !== 'Add New Book'" class="col-sm-3 control-label">Lend to a friend!</label>
                                <div ng-if="!hasFriend && state !== 'Add New Book'" class="col-sm-9">
                                    <select class="custom-select" ng-model="formData.friend_id" ng-options="friend.id as friend.name for friend in all_friends">
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(modalstate, book_id)">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="friendsDiv" class="section hide" ng-controller="friendsCtrl">

        <!-- Data table -->
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add',0)">Add New Friend</button></th>
                </tr>
              </thead>
              <tbody>
                 <tr ng-repeat="friend in friends">
                    <td>{{ $index + 1 }}</td>
                    <td>{{ friend.name }}</td>
                    <td>
                       <button class="btn btn-default btn-xs btn-detail" ng-click="showBooks(friend.books)">Show books</button>
                       <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit',friend.id)">Edit</button>
                       <button class="btn btn-danger btn-xs btn-delete" ng-click="confirmDelete(friend.id)">Delete</button>
                    </td>
                </tr>
            </tbody>
        </table>

        <!-- Modal for friend creating and editing -->
        <div class="modal fade" id="friendModal" tabindex="-1" role="dialog" aria-labelledby="friendModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="friendModalLabel">{{state}}</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="inputName" class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="inputName" placeholder="Friend name" value="{{name}}" ng-model="formData.name">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(modalstate, friend_id)">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="friendsBooks" role="dialog" aria-labelledby="showFriendsBooksLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="showFriendsBooksLabel">{{name}}</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <thead>
                                <th>Books with this friend</th>
                                <th></th>
                            </thead>
                            <tbody>
                                <tr ng-repeat="book in friends_books">
                                    <td>{{book.title}}</td>
                                    <td><button class="btn btn-danger btn-xs" ng-click="giveBookBack(book.id)">Give back</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Load Javascript Libraries (AngularJS, JQuery, Bootstrap) -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
      crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- There will be our javascript Application here -->
    <script type="text/javascript">
        var app = angular.module('bookLoan', []);

        app.factory('CommunicationService', function($rootScope){
            var mySharedService = {};

            mySharedService.updateBooks = function(){
                $rootScope.$broadcast('updateBooks');
            }

            mySharedService.updateFriends = function(){
                $rootScope.$broadcast('updateFriends');
            }

            return mySharedService;
        });

        /*************** BOOKS CONTROLLER *****************/
        app.controller('booksCtrl',['$scope', '$http', 'CommunicationService', function($scope, $http, CommunicationService){

            function listBooks(){
                /* get books */
                $http.get("/books").then(function(response) {
                    $scope.books = response.data.data;
                });
            }

            function formReset(){
                $scope.formData = {};
            }

            listBooks();

            $scope.save = function(modalstate,book_id){
                switch(modalstate){
                   case 'add': var url = "/books"; var method = 'POST'; break;
                   case 'edit': var url = "/books/"+book_id; var method = 'PUT'; break;
                   default: break;
                }
                $('#bookModal #btn-save').attr('disabled',true);
                $http({
                   method  : method,
                   url     : url,
                   data    : $.param($scope.formData),
                   headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).
                then(function(response){
                    $('#bookModal').modal('hide');
                    $('#bookModal #btn-save').attr('disabled',false);
                    formReset();
                    listBooks();
                    CommunicationService.updateFriends();
                }, function(response){
                    alert(response.data.error.message);
                    $('#bookModal #btn-save').attr('disabled',false);
                });
            }

            $scope.confirmDelete = function(id){
                var url = '/books';
                var isOkDelete = confirm('Is it ok to delete this?');
                if(isOkDelete){
                    $http({
                        method  : 'DELETE',
                        url     : url + '/' +id,
                        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    then(function(data){
                        listBooks();
                        CommunicationService.updateFriends();
                    }, function(data) {
                        alert('Unable to delete');
                    });
                } else {
                    return false;
                }
            }


            /* Show the modal */
            $scope.toggle = function(modalstate,id) {
                $scope.modalstate = modalstate;
                switch(modalstate){
                    case 'add':
                        $scope.state = "Add New Book";
                        $scope.formData = {};
                        $scope.formData.book_id = 0;
                        $scope.formData.title = '';
                        break;
                    case 'edit':
                        $scope.state = "Book Detail";
                        $scope.book_id = id;
                        $http.get("/friends").
                        then(function(friends){
                            $http.get("/books/" + id)
                            .then(function(response) {
                                $scope.all_friends = friends.data.data;
                                $scope.formData = response.data.data;
                                $scope.hasFriend = response.data.data.friend;
                            });
                        })
                       break;
                    default: break;
                }

                $('#bookModal').modal('show');
            }

            $scope.$on('updateBooks', listBooks);
        }])

        /*************** FRIENDS CONTROLLER *****************/
        app.controller('friendsCtrl',  ['$scope', '$http', 'CommunicationService', function($scope, $http, CommunicationService){

            function listFriends(){
                /* get friends */
                $http.get("/friends").then(function(response) {
                    $scope.friends = response.data.data;
                });
            }

            function formReset($scope){
                $scope.formData = {};
            }

            listFriends();

            $scope.save = function(modalstate,friend_id){
                switch(modalstate){
                   case 'add': var url = "/friends"; var method = 'POST'; break;
                   case 'edit': var url = "/friends/"+friend_id; var method = 'PUT'; break;
                   default: break;
                }
                $('#friendModal #btn-save').attr('disabled',true);
                $http({
                   method  : method,
                   url     : url,
                   data    : $.param($scope.formData),
                   headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).
                then(function(response){
                   $('#friendModal').modal('hide');
                   $('#friendModal #btn-save').attr('disabled',false);
                   formReset($scope);
                   listFriends();
                   CommunicationService.updateBooks();
                }, function(response){
                   alert(response.data.error.message);
                   $('#friendModal #btn-save').attr('disabled',false);
                });
            }

            $scope.confirmDelete = function(id){
                var url = '/friends';
                var isOkDelete = confirm('Is it ok to delete this?');
                if(isOkDelete){
                    $http({
                        method  : 'DELETE',
                        url     : url + '/' +id,
                        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    then(function(data){
                        listFriends();
                        CommunicationService.updateBooks();
                    }, function(data) {
                        alert('Unable to delete');
                    });
                } else {
                    return false;
                }
            }

            /* Show the modal */
            $scope.toggle = function(modalstate,id) {
                $scope.modalstate = modalstate;
                switch(modalstate){
                    case 'add':
                        $scope.state = "Add New Friend";
                        $scope.formData = {};
                        $scope.formData.name = '';
                        break;
                    case 'edit':
                        $scope.state = "Friend Detail";
                        $scope.friend_id = id;
                        $http.get("/friends/" + id)
                        .then(function(response) {
                           $scope.formData = response.data.data;
                        });
                       break;
                    default: break;
                }

                $('#friendModal').modal('show');

            }

            $scope.giveBookBack = function(id) {
                $http({
                    method  :'PUT',
                    url     :'/books/' + id,
                    data    : $.param({"friend_id":0}),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).
                then(function(response){
                    $('#friendsBooks').modal('hide');
                    listFriends();
                    CommunicationService.updateBooks();
                }, function(response){
                    alert('Book not given back');
                })
            }

            $scope.showBooks = function(books){
                $scope.friends_books = books;
                $('#friendsBooks').modal('show');
            }

            $scope.$on('updateFriends', listFriends);
        }])

        $('#bs-example-navbar-collapse-1 li').click(function(){
            var menuItem = $(this);
            var targetDiv = menuItem.data('target')
            $('#bs-example-navbar-collapse-1 li').removeClass('active');
            menuItem.addClass('active');
            $('.section').toggleClass('hide');

        })
    </script>
</body>
</html>