<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The book which this person is in possession of
     *
     */
    public function book()
    {
        return $this->hasMany('App\Book');
    }
}
