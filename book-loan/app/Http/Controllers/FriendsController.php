<?php

namespace App\Http\Controllers;

use App\Friend;
use Validator;
use Illuminate\Http\Request;
use App\Transformer\FriendTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class FriendsController extends Controller
{
    public function index()
    {
        return $this->collection(
            Friend::all(),
            new FriendTransformer()
        );
    }

    public function show($id)
    {
        try {
            return $this->item(Friend::findOrFail($id), new FriendTransformer());
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => [
                    'message' => 'Friend not found'
                ]
            ], 404);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => [
                    'message' => 'name is required'
                ]
            ], 422);
        }

        $friend = Friend::create($request->all());
        $data = $this->item($friend, new FriendTransformer());

        return response()->json($data, 201 , [
            'Location' => route('friends.show', ['id' => $friend->id])
        ]);
    }

    public function update(Request $request, $id)
    {
        try {
            $friend = Friend::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => [
                    'message' => 'Friend not found'
                ]
            ], 404);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => [
                    'message' => 'name is required'
                ]
            ], 422);
        }

        $friend->fill($request->all());
        $friend->save();

        $data = $this->item($friend, new FriendTransformer());

        return response()->json($data, 200);
    }

    public function destroy($id)
    {
        try {
            $friend = Friend::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => [
                    'message' => 'Friend not found'
                ]
            ],404);
        }

        $friend->delete();

        return response(null, 204);
    }
}