<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'friend_id'];

    /**
     * Get the friend in possession of the book
     *
     */
    public function friend()
    {
        return $this->belongsTo('App\Friend');
    }
}
