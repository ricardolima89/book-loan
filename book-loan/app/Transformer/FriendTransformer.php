<?php

namespace App\Transformer;

use App\Friend;
use League\Fractal\TransformerAbstract;

class FriendTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'books'
    ];

    /**
    * Transform an Friend model
    *
    * @param Friend $friend
    * @return array
    */
    public function transform(Friend $friend)
    {
        return [
            'id'        => $friend->id,
            'name'      => $friend->name,
            'books'      => $friend->book,
            'created'   => $friend->created_at->toIso8601String(),
            'updated'   => $friend->created_at->toIso8601String(),
        ];
    }

    /**
    * Transform related books
    *
    * @param Friend $friend
    * @return array
    */
    public function includeBooks(Friend $friend)
    {
        return isset($friend->book) ? $this->collection($friend->book, new BookTransformer()) : [];
    }
}