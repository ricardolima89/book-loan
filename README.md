### Book loan  ###

Control the books you lend to your friends.

### Setup ###

1. Install [Docker](https://docs.docker.com/engine/installation/) && [Docker Compose](https://docs.docker.com/compose/install/)

2. Clone project: `$ git clone git@bitbucket.org:ricardolima89/book-loan.git`

3. make sure no services are running on ports :80, :9000 and :3306

4. run `$ cd book-loan && ./setup.sh`

5. access http://localhost

### Testing ###

1. run `$ ./test.sh`