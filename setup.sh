#!/bin/bash

docker run --rm -v $(pwd)/book-loan:/app composer/composer install

cp -rf book-loan/. images/php/book-loan

docker-compose up --build --force-recreate -d
